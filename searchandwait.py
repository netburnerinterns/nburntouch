#!/usr/bin/python3
import sys, socket, traceback, struct, subprocess, time, termios, re, os, binascii, signal
from sys import stdin
from termios import tcflush, TCIFLUSH
from subprocess import Popen, PIPE
import getpass


resList = []
macList = []
userChoice = 0

def main() :
	searchandwait()
	print ("Booting Up Display")
		

def findmacConfig() :
	return os.path.exists('/home/nburn/macConfig.txt') 


def searchandwait() :
	host = ''
	port = 20034
	tfstruct = struct.Struct('!LB')
	
	s = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
	s.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
	s.setsockopt(socket.SOL_SOCKET, socket.SO_BROADCAST, 1)
	s.bind((host, port))
	# try and find all the NetBurner devices on the network
	try:
		data = struct.pack("!Lc",0x4255524E,b'R')
		s.sendto(data, ('<broadcast>', port))
		s.settimeout(2)
		while 1 :
			message, address = s.recvfrom(8192)
			if getMAC(message) is '' :  # filter null MAC addr
				continue 
			resList.append(address[0])  # store ip
			macList.append(getMAC(message))  # store MAC
		if not macList:
			noMac()
	# when its done trying to find check if default MAC addr is stored
	except  socket.error:
		errno, errstr = sys.exc_info()[:2]
		if errno == socket.timeout:
			if findmacConfig():
				verifyMAC()
				return
			else: 
				waiting()
			raise
	except:
	    print ("Something went wrong printing traceback ...")
	    traceback.print_exc()
					
	
def timesUp(signum, frame):
	print ("No interrupt has been made!")
	raise OSError (" ")  # not an error; pass up the exception to move on


# check that stored default MAC is still available on the network
def verifyMAC():
	TEN_SEC = 10
	f = open ('/home/nburn/macConfig.txt', 'r+')
	readMac = f.readline()

	for x in macList:
		if readMac == x:
			print ("configured MAC verified!")
			return

	print ("No MAC in the network matches configured default MAC;")
	print ("configuring new default MAC in 10 seconds unless interrupted...")
	print ("To Interrupt: Press ` ('backtick' by esc) and enter")
	# try and see if user wants to keep the old MAC even if its not on the net
	try:
		signal.signal(signal.SIGALRM, timesUp)
		signal.alarm(TEN_SEC)
		userchoice = 0
	
		userInterrupt = input()
	
		if userInterrupt == '`':
			reserveMac()
		print ("User Input: " + userInterrupt)
		print ("Starting set default MAC procedure...")
		signal.alarm(0)
		sys.exit()
	# otherwise set the new default MAC
	except:
		waiting()


# loop / halting function
def reserveMac():
	print ("You have chosen to reserve configured MAC address")
	print ("configured MAC address remain same, but the application cannot run")
	print ("to re-configure default MAC to boot interface, restart the system")
	while True:
		pass


# loop / halting function
def noMac():
	print ("No NetBurner devices is detected on this network")
	print ("Please connect at least one NetBurner device and restart this module")
	while True:
		pass		


# deciding default MAC addr
def waiting():
	global userChoice 
	TEN_SEC = 10
	i = 0

	# two cases that this func will be called
	print ("The default MAC does not match or default Config file does not exist!") 
	print ("Looking for NetBurner devices...")
	# print out list of MAC that is on the network 
	for x in resList: 
		print ("NetBurner Found: " + macList[i] + " @ " + x)
		i+=1
	# try to see if user wants default to be set to first MAC found
	# case 1. user interrupts with backtick -> give them a choices of MAC
	#      2. interrupts with anything else -> first MAC as default (skip)
	#      3. no interrupt                  -> first MAC as default
	try: 
		signal.signal(signal.SIGALRM, timesUp)
		signal.alarm(TEN_SEC)

		print ("==============================================================")
		print ("First MAC found is: " + macList[0])
		print ("Setting it as default MAC in 10 seconds unless interrupted: ")
		print ("To Interrupt: Press ` ('backtick' by esc) and enter")

		userInterrupt = input()

		userchoice = 0

		if userInterrupt == '`':
			userChoice = macChoices()

		signal.alarm(0)
		print ("User Input: " + userInterrupt)
		print ("Configuring default mac to: " + macList[userChoice])
		sys.exit()
	# then make the default MAC configure file
	except:
		print ("MAC Configured! Starting application!")
		makeConfig(macList[userChoice])
		subprocess.call("startx", shell = True);  # start display
		sys.exit() 


def makeConfig(macAddr):
	f = open ('/home/nburn/macConfig.txt', 'w')
	f.write(macAddr)
	f.close
	

# prints available choices and take input as integer value	
def macChoices():
	try:
		signal.alarm(0)
	except: 
		print ("==============================================================")
	j = 0

	while j < len(resList) :
		print ("NetBurner device #"+ str(j) + ": " + resList[j] + " from device: " + macList[j] )
		j += 1
	j -= 1

	print ("Choose a default device between # : 0 - " + str(j))

	while True:
		try :	
			userinput = int(stdin.readline().strip())
			print("You have chosen device #" + str(userinput));
			if 0 > userinput or userinput > j :
				raise ValueError('Not a valid input. Try again! Your input must be 0 < x <= ' + str(j))	
			return userinput	
		except ValueError as err:
			print ("Not a valid input. Try again! Your input must be 0 < x < " + str(j))

	
# parse out MAC addr from the message
def getMAC(message):
	mac = binascii.b2a_hex(message)[224:236].decode('ascii')
	mac = ":".join([mac[i:i+2] for i in range(0, len(mac), 2)])
	return mac

main()
