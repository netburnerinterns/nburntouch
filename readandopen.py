#!/usr/bin/python3
import sys, socket, traceback, struct, subprocess, time, termios, re, os, binascii
from sys import stdin
from termios import tcflush, TCIFLUSH
from subprocess import Popen, PIPE

resList = []
macList = []

def main():
	f = open ('./macConfig.txt', 'r')
	readMac = f.readline()
	(found_ip, found_mac) = findSpecificNburn(readMac)
	
	openBrowser(found_ip)
	f.close()

def findSpecificNburn(macAddr) : 
	host = ''                               # Bind to all interfaces
	port = 20034
	tfstruct = struct.Struct('!LB')

	s = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
	s.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
	s.setsockopt(socket.SOL_SOCKET, socket.SO_BROADCAST, 1)
	s.bind((host, port))

	try:
		data = struct.pack("!Lc",0x4255524E,b'R')
		s.sendto(data, ('<broadcast>', port))
		s.settimeout(2)
		while 1 :
			message, address = s.recvfrom(8192)
			if getMAC(message) is '' :
				continue 
			resList.append(address[0])
			macList.append(getMAC(message))

	except socket.error:
		errno, errstr = sys.exc_info()[:2]
		if errno == socket.timeout:
			if (macAddr is 'no_mac_set') :
				return resList[0], macList[0]
			else :
				i = 0
				while i < len(macList) :
					if macList[i] == macAddr :
						break
					i += 1
				return (resList[i], macList[i])
			

		

def getMAC(message):
	mac = binascii.b2a_hex(message)[224:236].decode('ascii')
	mac = ":".join([mac[i:i+2] for i in range(0, len(mac), 2)])
	return mac


def openBrowser (ipaddr):
	chromium = "chromium --app=http://" + ipaddr
	subprocess.call(chromium, shell = True);

main()
